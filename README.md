# Assemblage Loader

This Unreal 4.27 plugin is adding a button in the viewport toolbar to select a "assemblage" and turn it into a actual actor in the current scene.

Context: **Topologie d'un ailleurs**, a VR project by [Emile Pierret](https://www.facebook.com/emile.pierret).

## about "assemblage"

Assemblages are json fiel generated with `Resources\blender-scripts\structure_to_json.py`. This script detects and export the 4x4 matrices of primitives. These primitives must exported and imported separatly in Unreal.

## json structure

all values are expressed in blender coordinate system

```
{
"pieces" : [
	{
		"name": "bool_cube.003.frozen.001", 	// name of the object in blender tree
		"tmpl": "bool_cube.003.frozen_tmpl", 	// name of the template to be loaded in Unreal
		"pos": [x,y,z], 						// floats
		"scale": [x,y,z], 						// floats
		"rot": [x,y,z,w] 						// floats, quaternion
	},
	...
}
```