import bpy, json, mathutils

data = { 'pieces' : [] }

for obj in bpy.data.objects:
    wm = obj.matrix_world
    pos = wm.to_translation()
    rot = wm.to_quaternion()
    scale = wm.to_scale()
    np = obj.name.split( '.' )
    tmpl = ''
    if len(np) > 1:
        for i in range(0,len(np)-1):
            if tmpl != '':
                tmpl += '.'
            tmpl += np[i]
    else:
        tmpl = obj.name
    tmpl += '_tmpl'
        
    data['pieces'].append({
        'name': obj.name,
        'tmpl': tmpl,
        'pos': [pos.x,pos.y,pos.z],
        'scale': [scale.x,scale.y,scale.z],
        'rot': [rot.x,rot.y,rot.z,rot.w],
    })

f = open( bpy.path.abspath( '//assemblage_final_gate.json' ), 'w' )
f.write( json.dumps( data ) )
f.close()