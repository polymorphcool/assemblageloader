// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "LocalizationTargetTypes.h"
#include "Developer/DesktopPlatform/Public/IDesktopPlatform.h"
#include "Developer/DesktopPlatform/Public/DesktopPlatformModule.h"
#include "Interfaces/IMainFrameModule.h" 
#include "EditorDirectories.h"
#include "Misc/Paths.h"
#include "Modules/ModuleManager.h"

#include "Assemblage.h"

class FToolBarBuilder;
class FMenuBuilder;

class FAssemblageLoaderModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
	
	/** This function will be bound to Command. */
	void PluginButtonClicked();
	
private:

	void RegisterMenus();

	bool load_json(const FString& path);

	FString default_path = "/Game/";

private:
	TSharedPtr<class FUICommandList> PluginCommands;
};
