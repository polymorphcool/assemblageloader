#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Assemblage.generated.h"

//#define Assemblage_TICK

UCLASS()
class AAssemblage : public AActor
{
	GENERATED_BODY()

private:

public:
	// Sets default values for this actor's properties
	AAssemblage();
	AAssemblage(const FObjectInitializer& ObjectInitializer);

	//UFUNCTION(BlueprintCallable, meta = (DisplayName = "LoadPieces"), Category = "Utilities")
	//	bool LoadData(UPARAM(DisplayName = "Pieces json file path") FString json_file_path, UPARAM(DisplayName = "Verbose") bool verbose = false);

	//UFUNCTION(BlueprintCallable, meta = (DisplayName = "GetPieces"), Category = "Utilities|TArray")
	//	const TArray <FTDUAComplexPiece>& GetPieces() const;

	//UFUNCTION(BlueprintCallable, meta = (DisplayName = "SetTemplatePackage"), Category = "Utilities")
	//	void SetTemplatePackage(FString fpath);

	UPROPERTY()
	class USceneComponent* Scene;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//bool file_exists(const FString package_path, const FString name);

	//bool load_asset(const FAssetRegistryModule& registry, const FString package_path, const FString name, FAssetData& target);

};