// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Framework/Commands/Commands.h"
#include "AssemblageLoaderStyle.h"

class FAssemblageLoaderCommands : public TCommands<FAssemblageLoaderCommands>
{
public:

	FAssemblageLoaderCommands()
		: TCommands<FAssemblageLoaderCommands>(TEXT("AssemblageLoader"), NSLOCTEXT("Contexts", "AssemblageLoader", "AssemblageLoader Plugin"), NAME_None, FAssemblageLoaderStyle::GetStyleSetName())
	{
	}

	// TCommands<> interface
	virtual void RegisterCommands() override;

public:
	TSharedPtr< FUICommandInfo > PluginAction;
};
