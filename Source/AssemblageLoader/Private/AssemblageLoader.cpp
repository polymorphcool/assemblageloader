// Copyright Epic Games, Inc. All Rights Reserved.

#include "AssemblageLoader.h"
#include "AssemblageLoaderStyle.h"
#include "AssemblageLoaderCommands.h"
#include "Misc/MessageDialog.h"
#include "ToolMenus.h"

static const FName AssemblageLoaderTabName("AssemblageLoader");

#define LOCTEXT_NAMESPACE "FAssemblageLoaderModule"

void FAssemblageLoaderModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	
	FAssemblageLoaderStyle::Initialize();
	FAssemblageLoaderStyle::ReloadTextures();

	FAssemblageLoaderCommands::Register();
	
	PluginCommands = MakeShareable(new FUICommandList);

	PluginCommands->MapAction(
		FAssemblageLoaderCommands::Get().PluginAction,
		FExecuteAction::CreateRaw(this, &FAssemblageLoaderModule::PluginButtonClicked),
		FCanExecuteAction());

	UToolMenus::RegisterStartupCallback(FSimpleMulticastDelegate::FDelegate::CreateRaw(this, &FAssemblageLoaderModule::RegisterMenus));
}

void FAssemblageLoaderModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.

	UToolMenus::UnRegisterStartupCallback(this);

	UToolMenus::UnregisterOwner(this);

	FAssemblageLoaderStyle::Shutdown();

	FAssemblageLoaderCommands::Unregister();
}

void FAssemblageLoaderModule::PluginButtonClicked()
{
	//// Put your "OnButtonClicked" stuff here
	//FText DialogText = FText::Format(
	//						LOCTEXT("PluginButtonDialogText", "Add code to {0} in {1} to override this button's actions"),
	//						FText::FromString(TEXT("FAssemblageLoaderModule::PluginButtonClicked()")),
	//						FText::FromString(TEXT("AssemblageLoader.cpp"))
	//				   );
	//FMessageDialog::Open(EAppMsgType::Ok, DialogText);

	const void* ParentWindowWindowHandle = nullptr;
	IMainFrameModule& MainFrameModule = FModuleManager::LoadModuleChecked<IMainFrameModule>(TEXT("MainFrame"));
	const TSharedPtr<SWindow>& MainFrameParentWindow = MainFrameModule.GetParentWindow();
	if (MainFrameParentWindow.IsValid() && MainFrameParentWindow->GetNativeWindow().IsValid())
	{
		ParentWindowWindowHandle = MainFrameParentWindow->GetNativeWindow()->GetOSWindowHandle();
	}

	if (ParentWindowWindowHandle == nullptr) {
		return;
	}

	IDesktopPlatform* DesktopPlatform = FDesktopPlatformModule::Get();
	bool bOpened = false;
	TArray<FString> OutFileNames;
	if (DesktopPlatform)
	{
		auto DefaultPath = FEditorDirectories::Get().GetLastDirectory(ELastDirectory::GENERIC_OPEN);
		bOpened = DesktopPlatform->OpenFileDialog(
			ParentWindowWindowHandle,
			"Assemblage loader",
			DefaultPath,
			TEXT(""),
			"Assemblage file|*.json",
			EFileDialogFlags::Multiple,
			OutFileNames
		);
	}

	bOpened = (OutFileNames.Num() > 0);

	for (int i = 0; i < OutFileNames.Num(); ++i) {
		load_json( OutFileNames[i] );
	}

}

bool FAssemblageLoaderModule::load_json(const FString& path) {

	if (!FPaths::FileExists(path)) {
		UE_LOG(LogTemp, Error, TEXT("assemblage NOT found: %s"), *path);
		return false;
	}

	// trying to get current world
	UWorld* world = GEditor->EditorWorld;
	world = GEditor->GetEditorWorldContext().World();
	if (world == nullptr) {
		UE_LOG(LogTemp, Error, TEXT("world NOT found: %s"), *path);
		return false;
	}

	UE_LOG(LogTemp, Warning, TEXT("loading assemblage: %s"), *path);

	world->SpawnActor(AAssemblage::StaticClass());

	return true;

}

void FAssemblageLoaderModule::RegisterMenus()
{
	// Owner will be used for cleanup in call to UToolMenus::UnregisterOwner
	FToolMenuOwnerScoped OwnerScoped(this);

	{
		UToolMenu* Menu = UToolMenus::Get()->ExtendMenu("LevelEditor.MainMenu.Window");
		{
			FToolMenuSection& Section = Menu->FindOrAddSection("WindowLayout");
			Section.AddMenuEntryWithCommandList(FAssemblageLoaderCommands::Get().PluginAction, PluginCommands);
		}
	}

	{
		UToolMenu* ToolbarMenu = UToolMenus::Get()->ExtendMenu("LevelEditor.LevelEditorToolBar");
		{
			FToolMenuSection& Section = ToolbarMenu->FindOrAddSection("Settings");
			{
				FToolMenuEntry& Entry = Section.AddEntry(FToolMenuEntry::InitToolBarButton(FAssemblageLoaderCommands::Get().PluginAction));
				Entry.SetCommandList(PluginCommands);
			}
		}
	}
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FAssemblageLoaderModule, AssemblageLoader)