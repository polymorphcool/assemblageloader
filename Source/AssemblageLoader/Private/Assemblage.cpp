#include "Assemblage.h"

// Sets default values
AAssemblage::AAssemblage()
{

#ifdef Assemblage_TICK
	PrimaryActorTick.bCanEverTick = true;
#endif

}

AAssemblage::AAssemblage(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;

	Scene = ObjectInitializer.CreateDefaultSubobject<USceneComponent>(this, TEXT("Scene"));
	Scene->SetMobility(EComponentMobility::Movable);
	SetRootComponent(Scene);

#ifdef Assemblage_TICK
	PrimaryActorTick.bCanEverTick = true;
#endif

}

// Called when the game starts or when spawned
void AAssemblage::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AAssemblage::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}