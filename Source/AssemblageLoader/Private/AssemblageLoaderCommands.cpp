// Copyright Epic Games, Inc. All Rights Reserved.

#include "AssemblageLoaderCommands.h"

#define LOCTEXT_NAMESPACE "FAssemblageLoaderModule"

void FAssemblageLoaderCommands::RegisterCommands()
{
	UI_COMMAND(PluginAction, "AssemblageLoader", "Execute AssemblageLoader action", EUserInterfaceActionType::Button, FInputGesture());
}

#undef LOCTEXT_NAMESPACE
